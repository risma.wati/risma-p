<?php
class App {
	//controller, method and params default
	protected $controller = 'home';
	protected $method = 'index';
	protected $params = [];

	public function __construct() {
    
     $url = $this->parseURL();
    
    if (isset($url[0])) {    
    	 //cek file controller
        if(file_exists('../app/controllers/'. ucfirst($url[0]) . '.php')){
     	//jika ada maka isi $this->controller dengan array[0]
     	$this->controller = $url[0];
     	//hilangkan array[0]
     	unset($url[0]);
      
        }
    }
     require_once '../app/controllers/'. ucfirst($this->controller) .'.php';
     	$this->controller =new $this->controller;

     	if(isset ($url[1]) )
     	{
     		//cek method di dalam controller
     		if(method_exists($this->controller, $url[1])){
     			//jika ada maka isi $this->method dengan array[1]
     			$this->method = $url[1];
     			//hilangkan array[1];
     			unset($url[1]);
     		}
     	}
        
        if(!empty($url)){
        	//jika ada maka isi $this->params dengan array tersebut
        	$this->params = array_values($url);
        }

        call_user_func_array([$this->controller, $this->method], $this->params);
    }
	public function parseURL() {
		if(isset($_GET['url'])) {
		    $url = rtrim($_GET['url'], '/');
		    $url = filter_var($url, FILTER_SANITIZE_URL);
		    $url = explode('/', $url);
		    return $url;
		}

	}
} 

 ?>