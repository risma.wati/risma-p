<?php

class courses extends controller {

	public function index()
	{
		$data['courses'] = $this->model('coursesModel')->getAllCourses();
		[
			[
              'category' => 'category 01',
              'description' => 'description 01',
              'pricing' => 'pricing 01',
			],
			[
			  'category' => 'category 02',
              'description' => 'description 02',
              'pricing' => 'pricing 02',
			],
			[
			  'category' => 'category 03',
              'description' => 'description 03',
              'pricing' => 'pricing 03',
			],
			[
			  'category' => 'category 04',
              'description' => 'description 04',
              'pricing' => 'pricing 04',
			],
			[
			  'category' => 'category 05',
              'description' => 'description 05',
              'pricing' => 'pricing 05',
			],
		];
		$this->view('templates/header',$data);
		$this->view('courses/index',$data);
		$this->view('templates/footer',$data);
	}

	public function tambah($category= 'marketing', $pricing = 'free')
	{
		echo 'category'.$category.' pricing '.$pricing;
	}

	public function detail() {		
		$data['courses'] = $this->model('coursesModel')->getSingleCourses();
		$this->view('templates/header', $data);
		$this->view('courses/detail', $data);
		$this->view('templates/footer', $data);
	}
} 
 ?>