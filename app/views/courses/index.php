<table border=1 cellpadding="20">
  <thead style="text-align: center;">
    <th>No</th>
    <th>Category</th>
    <th>Description</th>
    <th>Pricing</th>
    <th>Aksi</th>
  </thead>
  <tbody>
    <?php $index = 1 ?>
    <?php foreach ($data['courses'] as $dt) : ?>
    <tr>
      <td><?= $index++ ?> </td>
      <td><?= $dt['category'] ?> </td>
      <td><?= $dt['description'] ?> </td>
      <td><?= $dt['pricing'] ?> </td>
      <td><a href="<?= BASEURL ?>/courses/detail">Detail</a></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>