<?php
session_start();

require "../koneksi.php";

if( isset($_COOKIE['id']) && isset($_COOKIE['username'])){
  $id = $_COOKIE['id'];
  $username = $_COOKIE['username'];

  $result = mysqli_query($koneksi, "select * from users WHERE id = '$id'");
  $row = mysqli_fetch_assoc($result);

  if( $username = $row['username']){
  $_SESSION['login'] = true;
}
}

if ( isset($_SESSION['login'])){
  header("Location: ../courses/index.php");
  exit;
}

if ( isset($_POST['login'])) {
  $username = $_POST['username'];
  $password = $_POST['password'];

  $result = mysqli_query($koneksi, "select * from users WHERE username = '$username'");

// cek username
  if(mysqli_num_rows($result) == 1) {

// cek password
  $row = mysqli_fetch_assoc($result);

  if( md5($password) == $row['password']) {
  $_SESSION['login'] = true;
  
  if( isset($_POST['remember'])){
  setcookie('id',$row['id'],time()+60);
  setcookie('username', $row['username'], time()+60);
}
  header('Location: ../courses/index.php');
  exit;
}
}
  $error = true;
}
?>



<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">

    <title>Login</title>
    <!-- Bootstrap core CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="../asset/css/Login.css" rel="stylesheet">
  </head>
<body>  
    <form class="form-signin" action="" method="post">
      <h1 class="h3 mb-3 font-weight-normal">Login</h1>
      <?php if (isset($error)) : ?>
      <p style="color: red; font-style: italic;">username atau password salah</p>
      <?php endif; ?>
      <label for="inputUsername" class="sr-only">Username</label>
      <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me" name="remember"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Login</button>
    </form>
  </body>
</html>