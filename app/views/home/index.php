<!DOCTYPE html>
<html>
<head>
	<title>little of document</title>
</head>
<body>
	
	<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="asset/css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">

	<title>Hello, world!</title>
  </head>
  <body>
	<nav class="navbar navbar-expand-lg navbar-light bg-white">
	<div class="container">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <a class="navbar-brand" href="#">
  	<img src="asset/img/logo.svg" alt="" loading="lazy">
  </a>

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Service</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Courses</a>
      </li>
       <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">About us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Contact</a>
      </li>
      	<button class="btn btn-primary"> Log in</button>
    </ul>
  </div>
</div>
</nav>
<!-- akhir navbar -->

<!-- Jumbotron -->
<div class="jumbotron jumbotron-fluid "d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
            <h1 class="display-4">Get Access to Unlimited Educational Resources. Everywhere, Everytime!</h1>
            <p class="lead">premium access to more than 10,000 resources ranging from courses, events e.t.c.</p>

            <button class="btn btn-primary">Get Access</button>
      </div>
    </div>
  </div>
</div>
<!-- akhir jumbotron -->

<!-- services -->
<section class="services d-flex align-items-center justify-content-center" id="services">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mb-3">
        <h4>Services</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4">
        <img src="asset/img/service1.svg" alt=""> <h6>Unlimited Access</h6>
        <p>One subscription unlimited access</p> 
      </div>
       <div class="col-lg-4">
        <img src="asset/img/service2.svg" alt=""> <h6>Expert Teachers</h6> <p>Learn from industry experts who are passionate about teaching</p>
      </div>
       <div class="col-lg-4">
        <img src="asset/img/service3.svg" alt=""> <h6>Learn Anywhere</h6> <p>Switch between your computer, tablet, or mobile device.</p>
      </div>
    </div>

  </div>
</section>
<!-- akhir services -->

<!-- Stories -->
<section class="stories d-flex align-items-center" id="stories">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <h1>Success Stories From Our Students Worldwide</h1> 
        <p>Semaj Afrika is an online education platfrom that delivers video courses, programs and resources for individual, Advertising & Media Specialist. Online Marketing Professionals, Freelancers and anyone looking to pursue a career in digital marketing. Accounting, Web development, Programming, Multimedia aand CAD design. </p>
        <button class="btn btn-primary ">Discover</button>
      </div>
        <div class="col-lg-8 text-center">
        <img src="asset/img/stories-img.png" align="" width="80%">
      </div>
    </div>
  </div>
</div>
</section>
<!-- akhir stories -->

<!-- Courses -->
<section class="courses d-flex align-items-center" id="courses">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h4>Courses</h4>
      </div>
    </div>
      <div class="row pb-4">
      <div class="col-lg-12 text-center">
        <a href="">All</a>
        <a href="">Design</a>
        <a href="">Web Development</a>
        <a href="">Diigital</a>
        <a href="">Photography</a>
        <a href="">Motion Graphics</a>
        <a href="">Digital Marketing</a>
      </div>
    </div>

    <div class="row">


<?php
include "koneksi.php";
$data = mysqli_query($koneksi, "select*from courses");
while($d = mysqli_fetch_array($data)) {
?>   
      <div class="col-lg-3">
        <div class="card">
          <div class="card-top text-center pt-2">
          <?php echo $d['pricing'] ?>
          </div>
          <img src="asset/img/<?php $d['img']?> " class="card-img-top" alt="...">
          <div class="card-body">
            <p><?php echo $d['description'] ?></p>
          </div>
        </div> 
      </div>
<?php

}
?>
      </div>
   <div class="row justify-content-center mt-5">
          <button class="btn btn-primary mb-5">Discover</button>
      </div>
</section>
<!-- akhir courses -->

<!-- achievement -->
<section class="achievement d-flex align-items-center justify-content-center" id="achievement">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mb-3">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3">
        <img src="asset/img/Achievement1.svg" alt=""><h6>5,679</h6>
        <p>Registered Students</p> 
      </div>
       <div class="col-lg-3">
        <img src="asset/img/Achievement2.svg" alt=""><h6>2,679</h6> <p>Student has been helped to achieve their dreams</p>
      </div>
       <div class="col-lg-3">
        <img src="asset/img/Achievement3.svg" alt=""><h6>10,000</h6> <p>More than 10,000 people visits our site monthly</p>
      </div>
      <div class="col-lg-3">
        <img src="asset/img/Achievement4.svg" alt=""><h6>#10</h6> <p>Ranked among the top 10 growing online learning startups in West Africa</p>
      </div>
    </div>
  </div>
</section>
<!-- akhir achievement -->

<!-- Testimonials -->
<section class="testimonials d-flex align-items-center" id="testimonials">
  <div class="container">
  <div class="row text-center">
    <div class="col-lg-12">
       <h1>What Student Say</h1>
    </div>
  </div>
  <div class="row text-center mb-3">
    <p>
      Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist.
    </p>
  </div>
  <div class="row">
    <div class="col-lg-3">
      <div class="card">
        <div class="card-body">
          <img src="asset/img/testi.svg" alt="">
          <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
          <div class="row">
            <div class="col-lg-3">
              <img src="asset/img/User-Pic.svg">
            </div>
            <div class="col-lg-8 pt-2 ml-2">
              <h5>Arthur Broklyn</h5>
              <p>Categories: 3d Modelling</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card">
        <div class="card-body">
          <img src="asset/img/testi.svg" alt="">
          <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
          <div class="row">
            <div class="col-lg-3">
              <img src="asset/img/User-Pic.svg">
            </div>
            <div class="col-lg-8 pt-2 ml-2">
              <h5>Arthur Broklyn</h5>
              <p>Categories: 3d Modelling</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card">
        <div class="card-body">
          <img src="asset/img/testi.svg" alt="">
          <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
          <div class="row">
            <div class="col-lg-3">
              <img src="asset/img/User-Pic.svg">
            </div>
            <div class="col-lg-8 pt-2 ml-2">
              <h5>Arthur Broklyn</h5>
              <p>Categories: 3d Modelling</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card">
        <div class="card-body">
          <img src="asset/img/testi.svg" alt="">
          <p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone.</p>
          <div class="row">
            <div class="col-lg-3">
              <img src="asset/img/User-Pic.svg">
            </div>
            <div class="col-lg-8 pt-2 ml-2">
              <h5>Arthur Broklyn</h5>
              <p>Categories: 3d Modelling</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- akhir testimonials -->

<!-- newslatter -->
<section class="newslatter d-flex align-items-center" id="newslatter">
  <div class="container">
     <img src="asset/img/Rectangle7.svg" alt="">
    <div class="row">
       <div class="col-lg-8">
       <h1>Subscribe to Our Newsletter</h1>
       <p>Get exclusive discounts and latest news deliverd to your inbox for free!</p>
       <button class="btn btn-primary"> Start Free Trial</button>
      </div>
    </div>
  </div>
</section>
<!-- akhir newslatter -->

<!-- footer -->
<section class="footer d-flex align-items-center pt-5" id="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
         <a class="navbar-brand" href="#">
          <img src="asset/img/logo.svg" alt="" loading="lazy">
        </a> 
        <p>Semaj Africa is an online education <br>
          platform that delivers video courses, <br>
          programs and resources.</p>
        <img src="asset/img/sosmed.svg" alt=""> 
      </div>
       <div class="col-lg-3">
        <h6>Quicklinks</h6> 
        <p>Home Courses <br> About Us <br>Contact Us</p>
      </div>
       <div class="col-lg-3">
        <h6>Contact Us</h6> 
        <p>(+55) 254. 254. 254 <br>Info@lsemajafrica.com <br>Helios Tower 75 Tam Trinh Hoang <br>Mai - Ha Noi - Viet Nam</p>
      </div>
      <div class="col-lg-3">
        <p>Terms and Conditions <br>Faq</p>
      </div>
    </div>

  </div>
</section>
<!-- akhir footer -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>
</body>
</html>