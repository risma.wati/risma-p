<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap core CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Dashboard</title>
  </head>
  <body>
      
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">PWEB</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL ?>/courses">Courses</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL ?>/testimonials">testimonials</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="../login/logout.php">logout</a>
      </li>
    </ul>
  </div>
</nav>
  
  <div class="container mt-5">
          <div class="row">
            <div class="col-lg-12">
              Daftar Testimonials
            </div>
            <div class="row">
              <table border=1 cellpadding="20">
                <thead style="text-align:center">
                  <th>No</th>
                  <th>Category</th>
                  <th>nama</th>
                  <th>testimonials</th>
                </thead>
                <tbody>
                  <?php $index = 1 ?>
                  <?php foreach ($data['testimonials'] as $dt) : ?>
                   <tr>
                    <td><?= $index++ ?> </td>
                    <td><?= $dt['category'] ?> </td>
                    <td><?= $dt['nama'] ?> </td>
                    <td><?= $dt['testimonials'] ?> </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>