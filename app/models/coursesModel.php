<?php  

class coursesModel {

	private $tabel = 'courses';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllCourses() 
	{
		// return $this->data;
		// $this->stmt = $this->dbh->prepare('SELECT*FROM courses');
		// $this->stmt->execute();
		// return $this->stmt->fetchAll(PDO::FETCH_ASSOC);

		$this->db->query('SELECT * FROM '. $this->tabel);
		return $this->db->resultSet();
	}

	public function getSingleCourses() {
		$this->db->query('SELECT * FROM '. $this->tabel);
		return $this->db->single();
	}
}

?>